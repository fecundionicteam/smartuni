import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeighingPage } from './weighing';

@NgModule({
  declarations: [
    WeighingPage,
  ],
  imports: [
    IonicPageModule.forChild(WeighingPage),
  ],
})
export class WeighingPageModule {}
