import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SchoolPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-school',
  templateUrl: 'school.html',
})
export class SchoolPage {
  uni = {}
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.uni = this.navParams.get("school");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SchoolPage');
  }

}
