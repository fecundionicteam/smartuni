import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  create_account(){
    this.navCtrl.push("SignupPage");
  }

  login(){
    this.navCtrl.setRoot("TabsPage");
  }

}
