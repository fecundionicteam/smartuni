import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class GetCoursesProvider {
  url = "http://127.0.0.1/smartuni/welcome/";
  headers: Headers;
  options: RequestOptions;

  constructor(public http: Http) {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    this.headers.append('Accept', 'application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }

  get_courses(){
    let desirable = "desirable";
    let essentials = "essentials";
    let relevants = "relevants";
    let body ="desirable="+desirable+"&essentials="+essentials+"&relevants="+relevants;
    return this.http.post(this.url +"get_possible_courses", encodeURI(body), this.options).map(res => res.json());
  }

  get_universities(){
    return this.http.get(this.url + "get_universities").map(res => res.json());
  }

}
