import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GetCoursesProvider } from '../../providers/get-courses/get-courses';


@IonicPage()
@Component({
  selector: 'page-course',
  templateUrl: 'course.html',
})
export class CoursePage {

  course = {}
  unis = []
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public getS: GetCoursesProvider) {
    this.course = this.navParams.get("course");
    console.log( this.course );
  }

  ionViewDidLoad() {
    this.getS.get_universities().subscribe( data => {
      this.unis = data;
    });
  }

  gotoschool(uni) {
    this.navCtrl.push("SchoolPage", { school: uni });
  }


}
