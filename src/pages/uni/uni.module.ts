import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UniPage } from './uni';

@NgModule({
  declarations: [
    UniPage,
  ],
  imports: [
    IonicPageModule.forChild(UniPage),
  ],
})
export class UniPageModule {}
