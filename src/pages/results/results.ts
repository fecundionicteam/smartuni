import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GetCoursesProvider } from '../../providers/get-courses/get-courses';

@IonicPage()
@Component({
  selector: 'page-results',
  templateUrl: 'results.html',
})
export class ResultsPage {

  res = {};
  courses = [];
  loading: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, public getS:GetCoursesProvider) {
    this.res = this.navParams.get("res");
  }

  ionViewDidLoad() {
    console.log( this.res );
    this.getS.get_courses().subscribe( data => {
      console.log(data);
      this.loading = false;
      this.courses = data;
    })
  }

  show_course(course){
    this.navCtrl.push("CoursePage", { course: course });
  }

}
