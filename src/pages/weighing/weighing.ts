import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-weighing',
  templateUrl: 'weighing.html',
})
export class WeighingPage {
  arts =[];
  points= [];
  subsidiaries=[];
  u = {
    so:"",
    st:"",
    sth:"",
    sub:"",
    sop:"A",
    stp:"A",
    sthp:"A",
    subp:"A",
    ds:"0",
    cs:"0",
    ps:"0",
    gp:"0"
  }// user slected data.
  sw = {
    A: 6, B: 5, C: 4, D: 3, E: 2, O: 1, F: 0 
  } //subject mark weight

  constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController) {
  }

  ionViewDidLoad() {
    this.get_subjects();
  }

  get_subjects(){
   this.arts = [
      "Mathematics",
      "History"	,
      "Economics",
      "Biology",
      "Entrepreneurship",
      "Physics",
      "Geography"	,
      "Chemistry",	
      "Fine Art",
    ];

    this.points = [
      "A", 
      "B", 
      "C", 
      "D", 
      "E", 
      "F", 
      "O"
    ]
    this.subsidiaries = ["Sub Computer Studies",
      "Sub-mathematics"]
  }

  calculate_weight(){
      console.log(this.u)
    console.log(this.sw[this.u.sop])
    let cater_one = (this.sw[this.u.sop] * 3)  
    + (this.sw[this.u.stp] * 3) 
    + (this.sw[this.u.sthp] * 2)  
    + (this.sw[this.u.subp] * 1) 
    + parseInt(this.u.gp) 
    + (parseInt(this.u.ds) * .3) 
    + (parseInt(this.u.ps) * .1) 
    + (parseInt(this.u.cs) * .2); 


      let cater_two = (this.sw[this.u.sop] * 3)
      + (this.sw[this.u.sthp] * 3)
      + (this.sw[this.u.stp] * 2)
      + (this.sw[this.u.subp] * 1)
      + parseInt(this.u.gp)
      + (parseInt(this.u.ds) * .3)
      + (parseInt(this.u.ps) * .1)
      + (parseInt(this.u.cs) * .2);

      let cater_three = (this.sw[this.u.sthp] * 3)
      + (this.sw[this.u.stp] * 3)
      + (this.sw[this.u.sop] * 2)
      + (this.sw[this.u.subp] * 1)
      + parseInt(this.u.gp)
      + (parseInt(this.u.ds) * .3)
      + (parseInt(this.u.ps) * .1 )
      + (parseInt(this.u.cs) * .2 ); 

      let res = [
        {
          sub: this.u.so + " And " + this.u.st ,
          mark: cater_one
        },
        {
          sub: this.u.sth + " And " + this.u.st,
          mark: cater_two
        },
        {
          sub: this.u.so + " And " + this.u.sth,
          mark: cater_three
        }
      ];
      console.log( res );
      this.navCtrl.push("ResultsPage", {res :res})

  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Smart Uni App Menu',
      buttons: [
        {
          text: 'About',
          handler: () => {
            this.navCtrl.push("AboutPage");
          }
        }, {
          text: 'Logout',
          handler: () => {
            console.log('Archive clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

}
