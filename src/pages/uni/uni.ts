import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController } from 'ionic-angular';
import { GetCoursesProvider } from '../../providers/get-courses/get-courses';

@IonicPage()
@Component({
  selector: 'page-uni',
  templateUrl: 'uni.html',
})
export class UniPage {
  unis = [];
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public actionSheetCtrl: ActionSheetController, 
    public getS: GetCoursesProvider) {
    this.get_universities();
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Smart Uni App Menu',
      buttons: [
        {
          text: 'About',
          handler: () => {
            this.navCtrl.push("AboutPage");
          }
        }, {
          text: 'Logout',
          handler: () => {
            console.log('Archive clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  get_universities(){
    this.getS.get_universities().subscribe( data => {
      this.unis = data;
    });
  }

   gotoschool(uni){
     this.navCtrl.push("SchoolPage", {school:uni});
   }

}
